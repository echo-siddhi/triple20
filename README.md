<div align="center">
    <h1>
        <br>
        <a href="#">
            <img src="https://gitlab.com/technowolf/triple20/raw/66e7a0bb78fde7c5fcba8d1c62a4059c6999f465/assets/project-logo.png" 
            alt="Nemean" width="200"></a>
        <br>
        Triple20
        <br>
    </h1>
    <h4 align="center">Triple20 is an application made for Developers,
    Designers and every other human being <br />to prevent eye strain caused 
    by spending most of their time in front of digital screens.</h4>
</div>
<div align="center">
    <a href="https://www.gnu.org/licenses/gpl-3.0.html" target="_blank">
        <img src="https://img.shields.io/badge/license-MIT-brightgreen.svg">
    </a>
    <a href="http://makeapullrequest.com" target="_blank">
        <img src="https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat">
    </a>
    <a href="https://gitlab.com/technowolf/triple20/commits/master">
        <img alt="pipeline status" src="https://gitlab.com/technowolf/triple20/badges/master/pipeline.svg" />
    </a>
    </a>
    <a href="https://saythanks.io/to/daksh7011" target="_blank">
        <img src="https://img.shields.io/badge/SayThanks.io-%E2%98%BC-1EAEDB.svg">
    </a>
    <a href="https://www.paypal.me/daksh7011" target="_blank">
        <img src="https://img.shields.io/badge/$-donate-ff69b4.svg?maxAge=2592000&amp;style=flat">
    </a>
    <br>
    <br>
</div>

# Overview
Triple20 is an Android app which is aimed to reduce the eye strain caused by spending more time with digital
screens. Triple20 will take care of health of your eyes and make sure you won't be victim of any problems and
complications of eye strain.

The idea of this project was came from a personal issue I faced in past due to spending 12+ hours of my day
on a computer screen because of my intense love for programming and gaming. I faced symptoms like
itching eyes, dry eyes and sometimes burning eyes followed by a headache. 
After facing such issue I did some digging about it and read a research paper about 20-20-20 rule, 
And then I had a thought that there are tons of people who spends a crazy time on digital screens. 
In order to prevent such vision problems I decided to develop an app to help the developers, designers
and anyone who spends most their time on a digital screen.

**I made this app OpenSource and 100% ads free since I had no plans gain any profit from this app because it was aimed for a greater good for humans.**


# Contribution Guide
Please take a look at the [contributing](CONTRIBUTING.md) guidelines if you're interested in helping by any means.

Contribution to this project is not limited to coding help, You can suggest a feature, help with docs, UI design 
ideas or even some typos. You are just an issue away. Don't hesitate to create an issue.

# Q&A
 * **What is 20-20-20 rule?** :
    Basically, every 20 minutes spent using a screen, you should try to look away at something that is 20 feet away from you for a total of 20 seconds. Remember distance doesn't matter here but time DOES!

 * **Why 20 seconds?** :
    It takes about 20 seconds for your eyes to completely relax. While you’re resting your eyes, it’s also a good idea to get up and grab a drink of water to keep yourself hydrated. If your body is hydrated, your eyes will be as well.

 * **What are the symptoms of Eye Strain?**
    1. dry eyes
    1. watery eyes
    1. burning eyes 
    1. blurred vision
    1. headaches
    1. soreness in the neck, shoulders, or back
    1. sensitivity to light
    1. trouble concentrating

  * **So, Whats an outlook?** :
    Eye strain from computers and other digital devices can be uncomfortable. Triple20 may help you avoid eye strain along with decreasing your time spent looking at screens. Even if the strain feels severe it will likely **NOT** cause permanent damage to your vision. And your symptoms should subside once you rest your eyes.


# Emailware

Triple20 is an emailware. Which means, if you liked using this app or has helped you in anyway, I'd like you send me an email 
on [daksh@technowolf.in](mailto:daksh@technowolf.in) about anything you'd want to say about this software. 
I'd really appreciate it! Plus I would be more than happy to know my initiative helped someone. :)


# License

[MIT License](LICENSE) Copyright (c) 2018 Daksh

 Triple20 is provided under terms of MIT license.

# Links

[Issue Tracker](https://gitlab.com/technowolf/triple20/issues)
