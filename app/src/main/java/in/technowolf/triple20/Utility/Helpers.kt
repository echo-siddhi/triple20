package `in`.technowolf.triple20.Utility

import android.content.Context
import android.util.Log
import android.widget.Toast

fun showToast(context:Context, message: String, length:Int){
    Toast.makeText(context,message,length).show()
}
fun logError(tag:String,message:String){
    Log.e(tag,message)
}
fun logVerbose(tag:String,message:String){
    Log.v(tag,message)
}
fun logDebug(tag:String,message:String){
    Log.d(tag,message)
}
fun logInfo(tag:String,message:String){
    Log.i(tag,message)
}