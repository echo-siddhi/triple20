package `in`.technowolf.triple20.Receivers

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Context.ALARM_SERVICE
import android.content.Intent
import android.media.RingtoneManager
import android.os.PowerManager

class RemainderReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        //TODO: Cleanup the onRecieve with a method call

        // This method is called when the BroadcastReceiver is receiving an Intent broadcast.
        val pm: PowerManager = context.getSystemService(Context.POWER_SERVICE) as PowerManager
        val wakelock: PowerManager.WakeLock =
            pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "WAKELOCK: Triple20 Remainder")


        wakelock.acquire()

        try {
            val notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
            val r = RingtoneManager.getRingtone(context.applicationContext, notification)
            r.play()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        wakelock.release()

    }

    fun startRemainder(context: Context) {
        val am = context.getSystemService(ALARM_SERVICE) as AlarmManager
        val intent = Intent(context, RemainderReceiver::class.java)

        val pendingIntent = PendingIntent.getBroadcast(
            context.applicationContext, 0, intent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )
        am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 1 * 60 * 1000L, pendingIntent)
    }

    fun stopRemainder(context: Context) {
        val intent = Intent(context, RemainderReceiver::class.java)
        val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val pendingIntent = PendingIntent.getBroadcast(
            context.applicationContext, 0, intent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )

        alarmManager.cancel(pendingIntent)
        pendingIntent.cancel()
    }

    fun notificationBuilder() {
        //TODO: add a notification builder
    }
}
