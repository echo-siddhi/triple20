package `in`.technowolf.triple20.Fragments

import `in`.technowolf.triple20.R
import `in`.technowolf.triple20.Receivers.RemainderReceiver
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button


class HomeFragment : Fragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val rootView = inflater.inflate(R.layout.fragment_home, container, false)
        val remainderReceiver = RemainderReceiver()

        val mStartTimer: Button = rootView.findViewById(R.id.btn_start_timer)
        val mStopTimer: Button = rootView.findViewById(R.id.btn_stop_timer)

        mStartTimer.setOnClickListener {
            remainderReceiver.startRemainder(rootView.context)
        }

        mStopTimer.setOnClickListener {
            remainderReceiver.stopRemainder(rootView.context)
        }



        return rootView
    }

}
